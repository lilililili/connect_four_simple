require 'pry'

class Board
  class IllegalMoveException < StandardError; end

  WIDTH = 7
  HEIGHT = 6

  def initialize
    @board = Array.new(HEIGHT) { Array.new(WIDTH) }
  end

  def print
    @board.each do |row|
      mapped = row.map do |space|
        space.nil? ? '.' : space 
      end
      puts mapped.join(' ')
    end
    puts ''
  end

  def play(character, x_index)
    validate_play(character, x_index)
    (HEIGHT-1).downto(0) do |y_index|
      if @board[y_index][x_index].nil?
        @board[y_index][x_index] = character
        return
      end
    end
  end

  def validate_play(character, x_index)
    fail IllegalMoveException, 'invalid x_index' if x_index < 0 || x_index > WIDTH - 1
    fail IllegalMoveException, 'invalid character' unless ['X', 'O'].include?(character)
    fail IllegalMoveException, 'illegal play, column is full' unless @board[0][x_index].nil?
  end

  def has_four
    (rows + columns + diagonals).each do |list|
      winner = four_in_a_row(list)
      return winner unless winner.nil?
    end
    nil
  end

  def rows
    0.upto(HEIGHT - 1).map { |y_index| @board[y_index] }
  end
  
  def columns
    0.upto(WIDTH - 1).map do |x_index|
      0.upto(HEIGHT - 1).map { |y_index| @board[y_index][x_index] }
    end
  end
  
  def diagonals
    # descending left to right
    left_to_right = -2.upto(WIDTH - 4).map do |x|
      (HEIGHT - 1).downto(0).map do |y|
        @board[y][x + (HEIGHT - y - 1)]
      end.compact
    end
    # descending right to left
    right_to_left = 3.upto(WIDTH + 1).map do |x|
      (HEIGHT - 1).downto(0).map do |y|
        @board[y][x - (HEIGHT - y - 1)]
      end.compact
    end
    left_to_right + right_to_left
  end

  def four_in_a_row(array)
    # ruby makes this very easy to do in a non-efficient way
    array.each_cons(4) do |items|
      return items.first if items.uniq.length == 1 and !items.first.nil?
    end
    nil
  end

  def valid?
    return false if count_fours > 1
    return false unless players_took_turns?
    true
  end

  def count_fours
    count = 0
    (rows + columns + diagonals).each do |list|
      list.each_cons(4) do |items|
        count += 1 if items.uniq.length == 1 and !items.first.nil?
      end
    end
    count 
  end

  def players_took_turns?  # presuming X started
    stacks = Array.new(WIDTH) { Array.new }
    (HEIGHT - 1).downto(0) do |y_index|
      0.upto(WIDTH - 1) do |x_index|
        piece = @board[y_index][x_index] 
        stacks[x_index] << piece unless piece.nil?
      end
    end
    
    turn = 'X'
    while stacks.any? { |stack| stack.length > 0 }
      move = shift_move(stacks, turn)
      return false if move.nil?
      turn = turn == 'X' ? 'O' : 'X'
    end
    return true
  end

  def shift_move(stacks, turn)
    stacks.each do |stack|
      next if stack.length == 0
      if stack[0] == turn
        return stack.shift
      end
    end
    nil
  end
end

def test_x_min
  board = Board.new
  begin
    board.play('X', -1)
  rescue Board::IllegalMoveException
    return puts 'ok'
  end
  fail 'illegal move worked'
end

def test_x_max
  board = Board.new
  begin
    board.play('X', 7)
  rescue Board::IllegalMoveException
    return puts 'ok'
  end
  fail 'illegal move worked'
end

def test_illegal_character
  board = Board.new
  begin
    board.play('Z', 0)
  rescue Board::IllegalMoveException
    return puts 'ok'
  end
  fail 'illegal move worked'
end

def test_full_column
  board = Board.new
  begin
    1.upto(7) { board.play('X', 0) }
  rescue Board::IllegalMoveException
    return puts 'ok'
  end
  fail 'illegal move worked'
end

def test_horizontal_win
  board = Board.new
  board.play('X', 0)
  board.play('X', 1)
  board.play('X', 2)
  fail '3 in a row' if board.has_four
  board.play('X', 3)
  fail 'has four' unless board.has_four
  puts 'ok'
end

def test_vertical_win
  board = Board.new
  board.play('X', 1)
  board.play('X', 1)
  board.play('X', 1)
  fail '3 in a row' if board.has_four
  board.play('X', 1)
  fail 'has four' unless board.has_four
  puts 'ok'
end

def test_diagonal_win
  board = Board.new
  board.play('X', 0)
  board.play('X', 0)
  board.play('X', 0)
  board.play('X', 1)
  board.play('X', 1)
  board.play('X', 2)
  board.play('O', 0)
  board.play('O', 1)
  board.play('O', 2)
  fail '3 in a row' if board.has_four
  board.play('O', 3)
  fail 'has four' unless board.has_four
  puts 'ok'
end

def test_count_fours
  board = Board.new
  board.play('X', 0)
  board.play('O', 1)
  board.play('X', 0)
  board.play('O', 1)
  board.play('X', 0)
  board.play('O', 1)
  board.play('X', 0)
  fail 'only 1 four' unless board.valid?
  board.play('O', 1)
  fail 'has two fours' if board.valid?
  puts 'ok'
end

def test_players_took_turns
  board = Board.new
  board.play('X', 0)
  board.play('X', 0)
  fail 'did not take turns' if board.players_took_turns?
  puts 'ok'
  
  board.play('X', 0)
  board.play('O', 6)
  board.play('X', 1)
  board.play('O', 5)
  board.play('X', 2)
  board.play('X', 3)
  fail 'did not take turns' if board.players_took_turns?
  puts 'ok'
end

if $PROGRAM_NAME == __FILE__
  test_x_min
  test_x_max
  test_illegal_character
  test_full_column
  test_horizontal_win
  test_vertical_win
  test_diagonal_win
  test_count_fours
  test_players_took_turns
end
